// BT1
document.querySelector("#btnShowNumber").onclick = function () {
  for (var i = 1, max = []; i <= 10; i++) {
    max.push(i);
  }
  for (var j = 0, ketqua = ""; j < 10; j++) {
    index = max;
    max = max.map((x) => x + 10);
    ketqua += `<tr>
    <th>Row ${j + 1}</th>
    <td>${index[0]}</td>
    <td>${index[1]}</td>
    <td>${index[2]}</td>
    <td>${index[3]}</td>
    <td>${index[4]}</td>
    <td>${index[5]}</td>
    <td>${index[6]}</td>
    <td>${index[7]}</td>
    <td>${index[8]}</td>
    <td>${index[9]}</td>
    </tr>
    <br>`;
  }
  document.querySelector("#ketQua1").innerHTML = ketqua;
  changeBtn();
};
function changeBtn() {
  if (document.getElementById("btnShowNumber")) {
    document.getElementById("btnShowNumber").innerText = "Ẩn bảng số từ 1-100";
    document
      .getElementById("btnShowNumber")
      .setAttribute("id", "btnHideNumber");
    document.getElementById("ketQua1").style.display = "table-row-group";
  } else {
    document.getElementById("btnHideNumber").innerHTML =
      "Hiện bảng số từ 1-100";
    document
      .getElementById("btnHideNumber")
      .setAttribute("id", "btnShowNumber");
    document.getElementById("ketQua1").style.display = "none";
  }
}

// BT2
function findPriNum(arrX) {
  var priNum = [];
  arrX.forEach(function (x) {
    for (var i = 2, count = 0; i <= x; i++) {
      if (x % i == 0) {
        count += 1;
      }
    }
    if (count < 2 && x > 1) {
      priNum.push(x);
    }
  });
  return priNum;
}
document.querySelector("#btnPriNum").onclick = function () {
  var num1 = document.querySelector("#inputNum1").value * 1;
  var num2 = document.querySelector("#inputNum2").value * 1;
  var num3 = document.querySelector("#inputNum3").value * 1;
  if (num1 == 0 || num2 == 0 || num3 == 0) {
    document.querySelector("#ketQua2").innerHTML = "Vui lòng nhập số khác 0";
  } else {
    var arrNum = [];
    arrNum = [num1, num2, num3];
    var priNum = findPriNum(arrNum);
    document.querySelector("#ketQua2").innerHTML = priNum;
  }
};

// BT3
function sumBT3(n) {
  var term1 = 0,
    term2 = 0,
    sum = 0;
  for (var i = 2; i <= n; i++) {
    term1 += i;
  }
  term2 = n * 2;
  sum = term1 + term2;
  return sum;
}
document.querySelector("#btnSumBT3").onclick = function () {
  var number = document.querySelector("#inputNumNBT3").value * 1;
  var sum = sumBT3(number);
  document.querySelector("#ketQua3").innerHTML = sum;
};

//BT4
function divisorBT4(n) {
  for (var divisor = [], i = 1; i <= n; i++) {
    if (n % i == 0) {
      divisor.push(i);
    }
  }
  return divisor;
}

document.querySelector("#btnDivisor").onclick = function () {
  var number = document.querySelector("#inputNumNBT4").value * 1;
  var divisor = divisorBT4(number);
  document.querySelector("#ketQua4").innerHTML = divisor;
};

// BT5
document.querySelector("#btnReversedNum").onclick = function () {
  var number = document.querySelector("#inputNumBT5").value;
  var reversedNumber = number.split("").reverse().toString().replace(/,/g, "");
  document.querySelector("#ketQua5").innerHTML = reversedNumber;
};
//BT6
document.querySelector("#btnFindNumX").onclick = function () {
  for (var sum = 0, i = 1, x = 0; true; i++) {
    sum += i;
    if (sum > 100) {
      break;
    }
    x = i;
  }
  document.querySelector("#ketQua6").innerHTML = x;
};

// BT7
function multiTable(n) {
  for (var ketQua7 = "", volume = 0, i = 0; i <= 10; i++) {
    volume = n * i;
    ketQua7 += n + " x " + i + " = " + volume + "<br>";
  }
  return ketQua7;
}
document.querySelector("#btnMultiTable").onclick = function () {
  var number = document.querySelector("#inputNumNBT7").value * 1;
  var result7 = multiTable(number);
  document.querySelector("#ketQua7").innerHTML = result7;
};

//BT8
document.querySelector("#btnAssignCard").onclick = function () {
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  var result8 = "";
  for (var player1 = [], i1 = 0; i1 < cards.length; i1 += 4) {
    player1.push(cards[i1]);
  }
  for (var player2 = [], i2 = 1; i2 < cards.length; i2 += 4) {
    player2.push(cards[i2]);
  }
  for (var player3 = [], i3 = 2; i3 < cards.length; i3 += 4) {
    player3.push(cards[i3]);
  }
  for (var player4 = [], i4 = 3; i4 < cards.length; i4 += 4) {
    player4.push(cards[i4]);
  }
  result8 =
    "player1: " +
    player1 +
    "<br>" +
    "player2: " +
    player2 +
    "<br>" +
    "player3: " +
    player3 +
    "<br>" +
    "player4: " +
    player4 +
    "<br>";
  document.querySelector("#ketQua8").innerHTML = result8;
};

//BT9
/**
 * a + b = 36 => a = 36 - b
 * (a * 4) + (b * 2) = 100
 * => (36 - b)* 4 + b * 2 = 100
 * => 144 - b * 4 + b * 2 = 100
 * => b * 2 = 44
 * => b = 22
 * => a = 14
 *
 */
function FindChickenAmout(m, n) {
  var chichkenAmount = 0;
  chichkenAmount = (4 * m - n) / 2;
  return chichkenAmount;
}
document.querySelector("#btnFindDogChickenAmount").onclick = function () {
  var m = document.querySelector("#inputNumDogChicken").value * 1;
  var n = document.querySelector("#inputTotalLegs").value * 1;
  var result9 = "";
  var dogAmount = 0;

  var chichkenAmount = FindChickenAmout(m, n);
  dogAmount = m - chichkenAmount;
  if (n % 2 !== 0 || n < m * 2) {
    result9 = "Vui lòng nhập số chân chính xác";
  } else {
    result9 = "Số gà: " + chichkenAmount + "<br>" + "Số chó: " + dogAmount;
  }
  document.querySelector("#ketQua9").innerHTML = result9;
};

//BT10
/**
 * gocGio = 30*inputhour
 * gocPhut = 6*inputminute
 * gocBonus = 0.5 * inputminute
 * VD: 11h, 25p
 * gocGio = 30*11 = 330
 * gocphut = 6*25 = 150
 * gocbonus = 0.5*25 = 12.5
 * goc ngoai = 330 - 150 + 12.5 = 192.5
 *
 */
document.querySelector("#btnCalDegClock").onclick = function () {
  var inputHours = document.querySelector("#inputHours").value * 1;
  var inputMinutes = document.querySelector("#inputMinutes").value * 1;
  var degHours = 30 * inputHours;
  var degMinutes = 6 * inputMinutes;
  var degBonus = 0.5 * inputMinutes;
  var degClock = degHours - degMinutes + degBonus;
  var degIn = 0;
  var degOut = 0;
  if (degClock < 180) {
    degIn = degClock;
    degOut = 360 - degIn;
  } else {
    degOut = degClock;
    degIn = 360 - degOut;
  }
  var result10 =
    "Góc trong: " + degIn + " độ" + "<br>" + "Góc ngoài: " + degOut + " độ";
  document.querySelector("#ketQua10").innerHTML = result10;
};
